from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todolist_list(request):
    todos = TodoList.objects.all()
    context = {
        "todos": todos,
    }
    return render(request, "todos/list.html", context)


def show_todo_detail(request, id):
    todo_detail = get_object_or_404(TodoList, id=id)

    context = {
        "todo_object": todo_detail,
    }
    return render(request, "todos/detail.html", context)


def create_todolist(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()

    context = {"form": form}
    return render(request, "todos/create.html", context)


def edit_todolist(request, id):
    todo_detail = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_detail)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todo_detail)

    context = {
        "todo_object": todo_detail,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def delete_todolist(request, id):
    todo_detail = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_detail.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def create_todoitem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        item = form.save()
        return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {"form": form}
    return render(request, "items/create.html", context)


def update_todoitem(request, id):
    item_detail = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item_detail)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)

    else:
        form = TodoItemForm(instance=item_detail)

    context = {
        "item_object": item_detail,
        "form": form,
    }
    return render(request, "items/edit.html", context)
